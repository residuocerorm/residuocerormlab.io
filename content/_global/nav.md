+++
# https://about.okkur.org/syna/fragments/nav/
fragment = "nav"
#disabled = true
date = "2021-05-09"
weight = 0
#background = ""
search = true
sticky = true

[breadcrumb]
  display = true # Default value is false
  #level = 0 # Default is 1
  background = "light"

# Branding options
[asset]
  image = "Logo_mzl_01_35x35.webp"
  text = "Logo ResiduoceroRM"
+++
