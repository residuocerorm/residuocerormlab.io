+++
fragment = "footer"
#disabled = true
date = "2019-11-01"
weight = 1200
#background = ""

menu_title = "Información adicional"

#[asset]
#  title = "Logo Title"
#  image = ""
#  text = "Logo ResiduoceroRM"
#  url = "#"

# 220x100
# https://about.okkur.org/syna/fragments/footer/

#<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.es">Creative Commons Atribución-CompartirIgual 4.0 Internacional</a> salvo que se especifique lo contrario.
#[![Licencia Creative Commons](https://licensebuttons.net/l/by-sa/4.0/88x31.png "Licencia Creative Commons Atribución-CompartirIgual 4.0 Internacional")](https://creativecommons.org/licenses/by-sa/4.0)
+++
#### Licencia
[Creative Commons Atribución-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es "CC-BY-SA") salvo que se especifique lo contrario.

<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0"><img alt="Licencia de Creative Commons" style="border-width:0" src="https://licensebuttons.net/l/by-sa/4.0/88x31.png" /></a>
<a rel="me" href="https://masto.es/@DavidMarzalC"></a>
<a rel="me" href="https://mastodon.social/@DavidMarzalC"></a>