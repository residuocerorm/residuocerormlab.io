+++
fragment = "copyright"
#disabled = true
date = "2019-10-27"
weight = 1250
#background = ""

copyright = "" # default: Copyright $Year .Site.params.name
attribution = true # enable attribution by setting it to true

# https://about.okkur.org/syna/fragments/copyright/
+++
