+++
fragment = "content"
#disabled = true
date = "2019-11-01"
weight = 100
#background = "secondary"
title = "Asociación Residuo Cero Región de Murcia"
#subtitle = ""

#[asset]
#  title = "Logo Title"
#  image = "Logo_mzl_02_219x219.webp"
#  text = "Logo ResiduoceroRM"
#  url = "#"

# ![Logo ResiduoceroRM](./Logo_mzl_02_219x219.webp "Lololo")
# https://spec.commonmark.org/0.29
+++

<p class="text-center">
<img width="219" height="219" src="/images/Logo_mzl_02_219x219.webp" alt="Logo asociación ResiduoCeroRM" >
</p>

Desde ResiduoCeroRM pensamos que es necesario unir fuerzas para hacer frente al problema medioambiental que la humanidad ha generado desde un punto de vista personal y colectivo, y por eso nos centramos en visibilizar los proyectos y recursos que hay disponibles en la Región.

---

Desde su creación formal en Marzo de 2018 con CIF G05500715, Residuo Cero Región de Murcia ha estado involucrada en:

* Participación y difusión en diferentes proyectos:
  * "Libera: Naturaleza sin basura", recogida de basura y residuos organizada por SEO BirdLife.
  * "Reto Río Limpio” organizado por la asociación Región de Murcia Limpia, concejalías de Infraestructuras, Obras y Servicios Públicos y de Modernización de la Administración y Desarrollo Urbano.
* Campaña para el fomento de bolsas reutilizables #YoUsoMiBolsa junto a distintas ONGs
  * Amigos de la Tierra, CECU, Desnuda la Fruta, Fundació Deixalles, Greenpeace, Orgranico, NastidePlastic, Red Ecofeminista, Retorna, Surfrider España y Vivir Sin Plásticos...
* Creación y mantenimiento de un [mapa](/proyectos "Mapa Residuo Cero Región de Murcia") interactivo con recursos y locales con enfoque en la sostenibilidad en la Región de Murcia
  * Tiendas a granel, de segunda mano, fuentes de agua potable, puntos de reciclaje, etc...
* Creación y mantenimiento de grupos de debate y consultorio de dudas en redes sociales.
* Campaña "10 Playas en 7 días", que consiste en la limpieza de varias playas de la región clasificando los residuos encontrados y concienciando a los veraneantes.
* Charlas sobre Zero Waste / Residuo Cero en:
  * Universidad Politécnica de Cartagena (UPCT), Federación de Asociaciones de Madres y Padres de Cartagena y Comarca (FAMPA Cartagena), Semana de la Salud del Ayuntamiento de Jumilla, colegios, etc...
* Talleres: Elabora tu propia pasta de dientes y desodorante.
* Mesas informativas en diferentes puntos y eventos de la Región de Murcia:
  * Alfonso XII (Murcia), Innovación óptica (Cartagena), Murcia Lan Party, III Feria de economía social y solidaria de Molina de Segura, Convivencia de AMPAs de Cartagena y Comarca

Puedes contactarnos en <ResiduoCeroRM@gmail.com>
