+++
fragment = "item"
#disabled = false
date = "2019-11-03"
weight = 110
background = "secondary"
align = "left"

title = "Mapa Residuo Cero Región de Murcia"
subtitle = "Todo los sitios, tiendas, empresas y lugares de interés"

# Subtitle pre and post item
#pre = "pre"
#post = "post"

[asset]
  image = "MapaRCRM_cuadrado.webp"
# No admite URL

[[buttons]]
  text = "Ver Mapa"
  url = "https://drive.google.com/open?id=1apf_4OOsPIB5Og1Y9qTTvCfHWYcEBJo6&usp=sharing"
  color = "primary"

#[[buttons]]
#  text = "Long Button"
#  url = "#"
#  color = "secondary"

# https://gohugo.io/content-management/formats/
+++

* Tiendas a granel de: comida, productos de limpieza, café, te, infusiones, cerveza...
* Empresas con criterios de sostenibilidad (locales y si no existen online)
* Tiendas de segunda mano o de alquiler.
* Reparación de calzado, ropa...
* Puntos limpios, de reciclaje y de deposito.
* Fuentes de agua potable en la Región.
