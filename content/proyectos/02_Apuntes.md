+++
fragment = "item"
#disabled = false
date = "2019-11-03"
weight = 120
background = "dark"
align = "right"

title = "Apuntes sobre Zero Waste"
subtitle = "Información para facilitarte llevar una vida más sostenible"

# Subtitle pre and post item
#pre = ""
#post = ""

[asset]
  image = "LogoReciclaje.webp"

[[buttons]]
  text = "Abrir apuntes"
  url = "https://docs.google.com/document/d/1wEl_dgYUrzY_8ZLbzk_94S8cCt8u452uXf3W3Cek99w/edit?usp=sharing"
  color = "primary"

#[[buttons]]
#  text = "Long Button"
#  url = "#"
#  color = "secondary"
+++

* Enlace a directorio de tiendas online con productos sostenibles.
* Otros mapas relacionados con el Residuo Cero.
* Comunidades.
* Guías de reciclaje.
* Preguntas frecuentes sobre como reciclar correctamente.
* Asociaciones y movimientos de la Región de Murcia con sus RRSS.
