+++
fragment = "embed"
#disabled = false
date = "2021-01-01"
weight = 130
background = "secondary"

title = "Canal y grupo de Telegram"
subtitle = "Eventos y noticias de la Región"
#title_align = "left" # Default is center, can be left, right or center

#media_source = "https://tttttt.me/s/ResiduoCeroRM"
media = '<iframe id="preview" class="embed-responsive-item" src="https://xn--r1a.website/s/ResiduoCeroRM" style="border: none; width: 100%; height: 400px;" title="Contenido del canal de Telegram"></iframe>'

responsive = false # prevent responsive behaviour
#ratio = "16by9" # 21by9, 16by9, 4by3, 1by1 - Default: 4by3
#size = "100" # 25, 50, 75, 100 (percentage) - default: 75

#<iframe id="preview" style="border:0px;height:500px;width:500px;margin:5px;box-shadow: 0 0 16px 3px rgba(0,0,0,.2);" src="https://xn--r1a.website/s/ResiduoCeroRM"></iframe>

# https://about.okkur.org/syna/fragments/embed/
+++
