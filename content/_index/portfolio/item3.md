+++
weight = 30
title = "Informacion Regional"
subtitle = "Canal con noticias y eventos"
item_url = "https://t.me/s/ResiduoCeroRM"

[asset]
  text = "Logo de Telegram"
  # No se puede usar SVGs
  image = "Telegram_2019_Logo_508x367.webp"
#https://commons.wikimedia.org/wiki/File:Telegram_2019_Logo.svg

[[labels]]
  title = "Telegram"
  icon = "fab fa-telegram-plane"

[[labels]]
  title = ""
  icon = "far fa-newspaper"

[[labels]]
  title = ""
  icon = "far fa-calendar-alt"

+++
