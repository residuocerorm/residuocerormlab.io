+++
weight = 20
title = "Mapa Residuo Cero RM"
subtitle = "Tiendas a granel, fuentes de agua..."
item_url = "https://drive.google.com/open?id=1apf_4OOsPIB5Og1Y9qTTvCfHWYcEBJo6&usp=sharing"

[asset]
  text = "Mapa RC Región de Murcia"
  image = "MapaRCRM_508x347.webp"

[[labels]]
  title = "Google map"
  icon = "fas fa-map-marked-alt"

[[labels]]
  title = ""
  icon = "fas fa-recycle"

[[labels]]
  title = ""
  icon = "fas fa-tint"

[[labels]]
  title = ""
  icon = "fas fa-store"

[[labels]]
  title = ""
  icon = "fas fa-shopping-basket"

[[labels]]
  title = ""
  icon = "fas fa-tshirt"

[[labels]]
  title = ""
  icon = "fas fa-coffee"

+++

En este mapa podrás encontrar todo lo relacionado con Residuo Cero en la Región de Murcia, como por ejemplo
