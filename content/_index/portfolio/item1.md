+++
weight = 10
title = "Apuntes e información"
subtitle = "Guia de reciclaje, recursos, asociaciones..."
item_url = "https://docs.google.com/document/d/1wEl_dgYUrzY_8ZLbzk_94S8cCt8u452uXf3W3Cek99w/edit?usp=sharing"

[[labels]]
  title = "Google doc"
  icon = "fas fa-info-circle"

[[labels]]
  title = ""
  icon = "fas fa-recycle"

[[labels]]
  title = ""
  icon = "fas fa-link"

[asset]
  text = "Logo de reciclaje"
  image = "LogoReciclaje_508x347.webp"

+++

Difusión de información y acciones de apoyo para la sensibilización en materia de residuos con el objetivo de su reducción en origen. Cuidar el planeta.
Ayudar a conseguir un planeta mejor y mejores condiciones para los que lo habitamos.
