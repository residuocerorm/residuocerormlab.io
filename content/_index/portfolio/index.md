+++
date = "2019-11-03"
fragment = "portfolio"
weight = 110
background = "secondary"

title = "Asociación Residuo Cero Región de Murcia"
subtitle = "Proyectos destacados"
#title_align = "left" # Default is center, can be left, right or center

#height = "110px" # Default is auto

# https://themes.gohugo.io//theme/syna/fragments/portfolio/
# 508x380 asset.image
# 348x348 / 348x264
+++
